<x-layout>

    {{-- navbar  --}}
   <x-navbar></x-navbar>

   @if(session('message'))
      <div  class="alert alert-success">
            {{session('message')}}
      </div>
   @endif

      {{-- carosello --}}
      <div id="carouselExampleCaptions " class="carousel slide " data-bs-ride="carousel">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="/img/dentista.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Soluzione definitiva per i tuoi denti.</h5>
              <p>Prima visita gratuita.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="/img/dentista.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Implantologia</h5>
              <p>Il programma implatare per te.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="/img/dentista.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Total clean</h5>
              <p>L'igiene dentale come non l'avevi mai vista.</p>
            </div>
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

  {{-- div con icone --}}
  <div class="container-fluid pd-top pd-bot">
      <div class="row col-12 my-5 text-center justify-content-center ">
          <div class="col-md-4 ">
            <i class="fas fa-tooth icondim"></i>
            <div class="col-12  text-center">
               <h4>Implantologia</h4>
            </div>
            <div class=" ">
            <p>Lorem ipsum, dolor sit amet consectetur <br> adipisicing elit. Dolor sit amet consectetur <br> adipisicing elit</p>
            </div>
          </div>
          <div class="col-12 col-md-4">
            <i class="fas fa-tooth icondim"></i>
            <div class=" ">
               <h4>White Experience</h4>
            </div>
            <div class="">
             <p>Lorem ipsum, dolor sit amet consectetur <br> adipisicing elit. Dolor sit amet consectetur<br> adipisicing elit</p>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <i class="fas fa-tooth icondim"></i>
            <div class="">
                <h4>Total Clean</h4>
            </div>
            <div class=" ">
                <p>lorem ipsum dolor sit amet, consectetur <br> adipiscicing elit.  Dolor sit amet consectetur <br> adipisicing elit</p>
            </div>
        </div>
      </div>
      <div class="row col-12 text-center">
        <div class=" col-md-4">
            <i class="fas fa-teeth-open icondim"></i>
          <div class=" ">
             <h4>Implantologia</h4>
          </div>
          <div class="">
          <p>Lorem ipsum, dolor sit amet consectetur <br> adipisicing elit. Dolor sit amet consectetur <br> adipisicing elit</p>
          </div>
        </div>
        <div class="col-12 col-md-4">
            <i class="fas fa-teeth-open icondim"></i>
          <div class=" ">
             <h4>White Experience</h4>
          </div>
          <div class="">
           <p>Lorem ipsum, dolor sit amet consectetur <br> adipisicing elit. Dolor sit amet consectetur <br> adipisicing elit</p>
          </div>
      </div>
      <div class="col-12 col-md-4">
        <i class="fas fa-teeth-open icondim"></i>
          <div class="">
              <h4>Total Clean</h4>
          </div>
          <div class="">
              <p>lorem ipsum dolor sit amet, consectetur <br> adipiscicing elit.  Dolor sit amet consectetur <br> adipisicing elit</p>
          </div>
      </div>
    </div>
  </div>
 
  {{-- team --}}
  <!-- Header -->
  <header class="bg-primary text-center py-5 mb-4 backgroundcol">
    <div class="container ">
      <h1 class="font-weight-light text-white">I nostri professionisti qualificati</h1>
    </div>
  </header>
  
  <!-- Pcontenuro pagina -->
  <div class="container-fluid pd-top ">
    <div class="row">
      <!-- Team Membro 1 -->
      @foreach ($members as $member)
 <div class="col-xl-3 col-md-6 mb-4 pd-bot">
    <div class="card border-0 shadow arrSchede">
      <img src="{{$member['img']}}" width="350px" height="350px" class="card-img-top" alt="...">
      <div class="card-body text-center ">
        <h5 class="card-title mb-0">{{$member['name']}}</h5>
        <div class="card-text text-black-50">{{$member['role']}}</div>
        <a class="btn btn-primary rounded-pill btn-nav-col" href="{{route('schedules', ['name'=>$member['name']])}}" role="button">info</a>
      </div>
    </div>
  </div>
 @endforeach
      {{-- <!-- Team Membro 2 -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-0 shadow arrSchede">
          <img src="/img/davide.jpg" width="350px" height="350px" class="card-img-top" alt="...">
          <div class="card-body text-center">
            <h5 class="card-title mb-0">Davide Cariola</h5>
            <div class="card-text text-black-50">Odontoiatra</div>
            <a class="btn btn-primary rounded-pill btn-nav-col" href="{{route('schedules2')}}" role="button">info</a>
          </div>
        </div>
      </div>
      <!-- Team Membro 3 -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-0 shadow arrSchede">
          <img src="/img/loris.jpg" width="350px" height="350px" class="card-img-top" alt="...">
          <div class="card-body text-center">
            <h5 class="card-title mb-0">Loris Pozzuoli</h5>
            <div class="card-text text-black-50">Odontoiatra</div>
            <a class="btn btn-primary rounded-pill btn-nav-col" href="{{route('schedules3')}}" role="button">info</a>
          </div>
        </div>
      </div>
      <!-- Team Membro 4 -->
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-0 shadow arrSchede">
          <img src="/img/carro.jpg" width="350px" height="350px" class="card-img-top" alt="...">
          <div class="card-body text-center">
            <h5 class="card-title mb-0">Edoardo Agus</h5>
            <div class="card-text text-black-50">Ortodontista</div>
            <a class="btn btn-primary rounded-pill btn-nav-col" href="{{route('schedules4')}}" role="button">info</a>
          </div>
        </div>
      </div>
    </div> 
    <!-- /.row -->
  
  
  <!-- /.container -->
 
{{-- prenota la tua visita --}}
 <x-header></x-header>
{{-- Grafico figo --}}
  <div class="container-fluid col-12 my-5 pd-top">
    <div class="row">
<div class="col-md-6">
<h3>Il nostro non è solo un mestiere ma anche una passione</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus ipsum modi facilis tempore possimus pariatur rem quis! Expedita nihil facere dicta provident perspiciatis perferendis? Reprehenderit asperiores ad doloremque ipsum dignissimos?</p>
</div>

<div class="col-md-6">
  <p>
    Total clean
  </p>
  <div class="progress mb-3 rounded-pill">
    <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <p>
    White experience
  </p>
  <div class="progress mb-3 rounded-pill">
    <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
  <p>
    Impiantologia
  </p>
  <div class="progress mb-3 rounded-pill">
    <div class="progress-bar " role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
  </div>
</div>
    </div>
  </div>

  {{-- offertina --}}
  <section class="pricing py-5 ">
    <div class="container ">
      <div class="row">
       {{-- grazie bootstrap --}}
        <div class="col-lg-4 ">
          <div class="card mb-5 mb-lg-0 shadow arrSchede">
            <div class="card-body">
              <h4 class="card-title text-muted text-uppercase text-center">Trattamento Total</h4>
              <h6 class="card-price text-center">€69</h6>
              <hr>
             <p>Trattamento total clean + 1 in omaggio</p>
              <a href="#" class="btn btn-block btn-primary text-uppercase rounded-pill btn-nav-col">Info</a>
            </div>
          </div>
        </div>
      {{-- per i tuoi snipets --}}
        <div class="col-lg-4 ">
          <div class="card mb-5 mb-lg-0 shadow arrSchede">
            <div class="card-body">
              <h4 class="card-title text-muted text-uppercase text-center">Sbiancamento</h4>
              <h6 class="card-price text-center">€320 000</h6>
              <hr>
              <ul class="fa-ul">
               <p>1 Sbiancamento White Experience Silver + 1 Pulizia dei Denti Total Clean a €320 000 + dente in diamante</p>
              <a href="#" class="btn btn-block btn-primary text-uppercase rounded-pill btn-nav-col">Info</a>
            </div>
          </div>
        </div>
        {{-- meravigliosi --}}
        <div class="col-lg-4 ">
          <div class="card shadow arrSchede">
            <div class="card-body">
              <h4 class="card-title text-muted text-uppercase text-center">Full-treatment</h4>
              <h6 class="card-price text-center">€380</h6>
              <hr>
              <p>Full treatment + scampagnata in montagna</p>
              <a href="#" class="btn btn-block btn-primary text-uppercase rounded-pill btn-nav-col">Info</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  {{-- trova parcheggio --}}
  <div class="container-fluid my-5  ">
    <div class="row text-center pd-top">
  <h3>Trova parcheggio facilmente!</h3>
    </div>
  </div>

  {{-- mappa --}}
  <section class="pb-5 pt-0 ">
    <div class="container-fluid p-0 pb-3">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23760.63032843802!2d12.450561961480778!3d41.89116257013662!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132f6042790b8f0d%3A0x6eea843bb65957d5!2sOspedale%20Pediatrico%20Bambino%20Ges%C3%B9!5e0!3m2!1sit!2sit!4v1618010855970!5m2!1sit!2sit" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
</section>

{{-- foooterino --}}
<x-footer></x-footer>

</x-layout>