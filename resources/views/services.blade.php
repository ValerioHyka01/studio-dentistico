<x-layout>
<x-navbar></x-navbar>
<header class="bg-primary text-center py-5 mb-4 backgroundcol ">
    <div class="container-fluid cardsDent">
      <h1 class="font-weight-light text-white">I nostri servizi</h1>
    </div>
  </header>
  <div class="container-fluid">
    <div class="row col-12 my-5 text-center justify-content-center ">
        <div class="col-md-4 ">
          <i class="fas fa-tooth icondim"></i>
          <div class="col-12  text-center">
             <h4>Implantologia</h4>
          </div>
          <div class=" ">
          <p>Lorem ipsum, dolor sit amet consectetur <br> adipisicing elit. Dolor sit amet consectetur <br> adipisicing elit</p>
          </div>
        </div>
        <div class="col-12 col-md-4">
          <i class="fas fa-tooth icondim"></i>
          <div class=" ">
             <h4>White Experience</h4>
          </div>
          <div class="">
           <p>Lorem ipsum, dolor sit amet consectetur <br> adipisicing elit. Dolor sit amet consectetur<br> adipisicing elit</p>
          </div>
      </div>
      <div class="col-12 col-md-4">
          <i class="fas fa-tooth icondim"></i>
          <div class="">
              <h4>Total Clean</h4>
          </div>
          <div class=" ">
              <p>lorem ipsum dolor sit amet, consectetur <br> adipiscicing elit.  Dolor sit amet consectetur <br> adipisicing elit</p>
          </div>
      </div>
    </div>
    <div class="row col-12 text-center">
      <div class=" col-md-4">
          <i class="fas fa-teeth-open icondim"></i>
        <div class=" ">
           <h4>Implantologia</h4>
        </div>
        <div class="">
        <p>Lorem ipsum, dolor sit amet consectetur <br> adipisicing elit. Dolor sit amet consectetur <br> adipisicing elit</p>
        </div>
      </div>
      <div class="col-12 col-md-4">
          <i class="fas fa-teeth-open icondim"></i>
        <div class=" ">
           <h4>White Experience</h4>
        </div>
        <div class="">
         <p>Lorem ipsum, dolor sit amet consectetur <br> adipisicing elit. Dolor sit amet consectetur <br> adipisicing elit</p>
        </div>
    </div>
    <div class="col-12 col-md-4">
      <i class="fas fa-teeth-open icondim"></i>
        <div class="">
            <h4>Total Clean</h4>
        </div>
        <div class="">
            <p>lorem ipsum dolor sit amet, consectetur <br> adipiscicing elit.  Dolor sit amet consectetur <br> adipisicing elit</p>
        </div>
    </div>
  </div>
</div>
<div class=" mx-auto ">
    <div class="row mx-auto">
      <!-- Team Membro  -->
 @foreach ($services as $service)
 <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-0 shadow arrSchede">
      <img src="{{$service['img']}}" width="350px" height="350px" class="card-img-top" alt="...">
      <div class="card-body text-center">
        <h5 class="card-title mb-0">{{$service['service']}}</h5>
        <div class="card-text text-black-50">{{$service['offered']}}</div>
        <a class="btn btn-primary rounded-pill btn-nav-col" href="{{route('servicesDett', ['service'=>$service['service']])}}" role="button">info</a>
      </div>
    </div>
  </div>
 @endforeach
          </div>
        </div>
  <x-footer></x-footer>
</x-layout>