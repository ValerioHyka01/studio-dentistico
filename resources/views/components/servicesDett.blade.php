<x-layout>
    <x-navbar></x-navbar>
    <div class="container-fluid text-center d-flex justify-content-center">
        <div class="row col-12 cardsDent mx-auto my-auto ">
          <div class="col-md-4 d-flex justify-content-center ">
            <div class="card" style="width: 18rem;">
              <div class="card-body shadow graphCard">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              </div>
            </div>         
          </div>
          <div class="col-md-4 d-flex justify-content-center ">
            <div class="card" style="width: 18rem;">
              <div class="card-body shadow graphCard ">
                <h5 class="card-title">Orari</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              </div>
            </div>
          </div>
          <div class="col md-4 d-flex justify-content-center ">
            <div class="card" style="width: 18rem;">
              <div class="card-body shadow graphCard">
                <h5 class="card-title">Sei indeciso?</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container-fluid ">
        <div class="row col-12  cardHeightFromTop ">
          <div class=" mb-3 col-12 shadow cardImRegul ms-3" style="max-width: 540px;">
            <div class="row g-0">
              <div class="col-md-4 col-md-6">
                <img src="/img/franco.png" class="cardinfo cardImage ms-2" style="max-width: 540px;" alt="...">
              </div>      
            </div>
          </div>
          <div class="col-md-6 mx-5">
            <h5>{{$service['service']}}</h5>
            
            <p>{{$service['description']}}</p>
            </div>
          </div>
        </div>
        <x-footer></x-footer>
    </x-layout>