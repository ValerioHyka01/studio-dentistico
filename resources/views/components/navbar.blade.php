<nav class="navbar navbar-expand-lg navbar-light shadow fixed-top bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">Tooth<span class="title">Masters</span></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active nav-btn" aria-current="page" href="{{route('homepage')}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active nav-btn" aria-current="page" href="{{route('team')}}">Chi siamo?</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active nav-btn" aria-current="page" href="{{route('services')}}">Servizi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active nav-btn" aria-current="page" href="{{route('contatti')}}">Contatti</a>
          </li>
        </ul>
        <form class="d-flex">
            <button class="btn btn-outline-success btn-nav-col rounded-pill mx-3" type="submit">Prenota ora</button>
          <button class="btn btn-outline-success btn-nav-col rounded-pill" type="submit">Scopri di più</button>
        </form>
      </div>
    </div>
  </nav>