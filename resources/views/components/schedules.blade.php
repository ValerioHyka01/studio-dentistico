
       <x-layout>
        <x-navbar></x-navbar>
        @props(['name', 'role', 'description' ])
        <div class="container-fluid text-center d-flex justify-content-center">
            <div class="row col-12 cardsDent mx-auto my-auto ">
              <div class="col-md-4 d-flex justify-content-center ">
                <div class="card" style="width: 18rem;">
                  <div class="card-body shadow graphCard">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>         
              </div>
              <div class="col-md-4 d-flex justify-content-center ">
                <div class="card" style="width: 18rem;">
                  <div class="card-body shadow graphCard ">
                    <h5 class="card-title">Orari</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>
              </div>
              <div class="col md-4 d-flex justify-content-center ">
                <div class="card" style="width: 18rem;">
                  <div class="card-body shadow graphCard">
                    <h5 class="card-title">Sei indeciso?</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="container-fluid ">
            <div class="row col-12  cardHeightFromTop ">
              <div class=" mb-3 col-12 shadow cardImRegul ms-3" style="max-width: 540px;">
                <div class="row g-0">
                  <div class="col-md-4 col-md-6">
                    <img src="/img/franco.png" class="cardinfo cardImage ms-2" style="max-width: 540px;" alt="...">
                  </div>      
                </div>
              </div>
              <div class="col-md-6 mx-5">
                <h5>Alessandro Franco</h5>
                <h6>Infermiere</h6>
                <p>Laureato in Odontoiatria e Protesi Dentaria presso l’Università “La Sapienza” di Roma. È iscritto all’Albo dei Medici Chirurghi e Odontoiatri di Roma con il numero 3937.
                  Inizia a lavorare come Odontoiatra presso il prestigioso Studio Dentistico Papa, fondato dal nonno paterno nel lontano 1932.
                  Dopo la Laurea frequenta numerosi Corsi di Perfezionamento in Conservativa Estetica, Protesi Fissa, Protesi Estetica, Gnatologia, Chirurgia Tradizionale ed Implantare.
                  Quasi 20 anni di esperienza in sala operatoria lo rendono un esperto in campo chirurgico odontoiatrico, implantare e protesico.
                  In un’ottica sempre più attenta alla perfezione sia qualitativa che estetica dei risultati, diventa Invisalign Provider, una certificazione che gli consente di effettuare i trattamenti ortodontici mediante l’uso degli allineatori trasparenti Invisalign.
                  Dopo i numerosi anni di lavoro presso lo studio di famiglia, le consulenze e le numerose collaborazioni con i più importanti network dentali, decide, insieme al fratello Gianmaria, di aprire un nuovo studio organizzato in maniera moderna in cui mettere a frutto i numerosi anni di esperienza, le conoscenze trasmesse da generazione in generazione ed i continui aggiornamenti professionali in modo da fornire al paziente un mix unico di innovazione e tradizione.</p>
                  <p>Chirurgia implantare</p>
                  <div class="progress mb-3 rounded-pill">
                    <div class="progress-bar" role="progressbar" style="width: 91%" aria-valuenow="91" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <p>Chirurgia conservativa</p>
                  <div class="progress mb-5 rounded-pill">
                    <div class="progress-bar " role="progressbar" style="width: 87%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                </div>
              </div>
            </div>
    
        <x-footer></x-footer>
    </x-layout>
         