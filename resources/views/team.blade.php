<x-layout>
<x-navbar></x-navbar>
<header class="bg-primary text-center py-5 mb-4 backgroundcol ">
    <div class="container-fluid cardsDent">
      <h1 class="font-weight-light text-white">I nostri professionisti qualificati</h1>
    </div>
  </header>
  
  <!-- Pcontenuro pagina -->
  <div class=" mx-auto ">
    <div class="row mx-auto">
      <!-- Team Membro 1 -->
 @foreach ($members as $member)
 <div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-0 shadow arrSchede">
      <img src="{{$member['img']}}" width="350px" height="350px" class="card-img-top" alt="...">
      <div class="card-body text-center">
        <h5 class="card-title mb-0">{{$member['name']}}</h5>
        <div class="card-text text-black-50">{{$member['role']}}</div>
        <a class="btn btn-primary rounded-pill btn-nav-col" href="{{route('schedules', ['name'=>$member['name']])}}" role="button">info</a>
      </div>
    </div>
  </div>
 @endforeach
          </div>
        </div>
      

<x-footer></x-footer>

</x-layout>