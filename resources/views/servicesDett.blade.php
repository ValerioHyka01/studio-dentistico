<x-layout>
    <x-navbar></x-navbar>
    <div class="container-fluid text-center d-flex justify-content-center">
        <div class="row col-12 cardsDent mx-auto my-auto ">
            <div class="col-md-4 d-flex justify-content-center ">
                <div class="card" style="width: 18rem;">
                    <div class="card-body shadow graphCard">
                        <h5 class="card-title">Recensioni</h5>
                        <h6>Lorenzo Aielli</h6>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the card's content.</p>
                            <i class="fas fa-star">4.5/5</i>
                    </div>
                </div>
            </div>
            <div class="col-md-4 d-flex justify-content-center ">
                <div class="card" style="width: 18rem;">
                    <div class="card-body shadow graphCard ">
                        <h5 class="card-title">Orari</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the card's content.</p>
                            <ul>
                                <li>
                                  <p>Lun-Ven: 9:30/19:00</p>
                                </li>
                                <li>Sab-Dom: 9:00/13:00</li>
                              </ul>
                    </div>
                </div>
            </div>
            <div class="col md-4 d-flex justify-content-center ">
                <div class="card" style="width: 18rem;">
                    <div class="card-body shadow graphCard">
                        <h5 class="card-title">Sei indeciso?</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the card's content.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row col-12  cardHeightFromTop ">
            <div class=" mb-3 col-12 shadow cardImRegul ms-3" style="max-width: 540px;">
                <div class="row g-0">
                    <div class="col-md-4 col-md-6">
                        <img src="{{$service['img']}}" class="cardinfo cardImage ms-2" style="max-width: 540px;" alt="...">
                    </div>
                </div>
            </div>
            <div class="col-md-6 mx-5">
                <h5>{{$service['service']}}</h5>

                <p>Nei centri ToothMasters scegliamo sempre il meglio delle tecnologie per i nostri pazienti ed è così anche per i trattamenti di estetica dentale. Nei nostri centri utilizziamo le Lumineers ovvero faccette in ceramica integrale ultrasottili, resistenti ed estetiche e soprattutto realizzate su misura. Perché sono uniche come il tuo sorriso? Perchè vengono applicate sui tuoi denti dopo una preparazione rapida e minimamente invasiva, per darti il sorriso dei tuoi sogni, bello, duraturo e naturale.  Le Faccette Lumineers proteggono i tuoi denti, oltre a darti un sorriso nuovo.

                    L’incredibile risultato che offrono le faccette Lumineers è frutto dell’utilizzo di un’innovativa ceramica brevettata. Le faccette si adattano alle tue esigenze di sorriso e colore, e non subiscono modifiche nel tempo.</p>
            </div>
        </div>
    </div>
    <x-footer></x-footer>
</x-layout>