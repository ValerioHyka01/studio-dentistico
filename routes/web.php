<?php
use App\Http\Controllers\PubblicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PubblicController::class, 'homepage'])->name('homepage');

Route::get('/team', [PubblicController::class, 'chiSiamo'])->name('team');

Route::get('/team/schedules/{name}', [PubblicController::class, 'schede'])->name('schedules');

Route::get('/servicesDett/{service}', [PubblicController::class, 'servizi'])->name('servicesDett');

Route::get('/services', [PubblicController::class, 'servGen'])->name('services');

Route::get('/contatti', [PubblicController::class, 'contacts'])->name('contatti');
Route::post('/contatti/submit', [PubblicController::class, 'submit'])->name('contatti.submit');

