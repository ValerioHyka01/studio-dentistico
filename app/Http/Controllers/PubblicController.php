<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail; 

class PubblicController extends Controller
{
    public function homepage(){
        $members = [
            ['name'=>'Alessandro Franco', 'role'=>'Infermiere', 'desciption'=>'descrizione', 'img'=>'/img/medico2.jpg'],
            ['name'=>'Davide Cariola', 'role'=>'Odontoiatra', 'desciption'=>'descrizione', 'img'=>'/img/team.jpg'],
            ['name'=>'Edoardo Agus', 'role'=>'Odontoiatra', 'desciption'=>'descrizione', 'img'=>'/img/supermedico.jpg'],
            ['name'=>'Loris Pozzuoli', 'role'=>'Odontoiatra', 'desciption'=>'descrizione', 'img'=>'/img/dottorino.jpg'],
        ];
        return view('homepage', ['members'=>$members]);
    }

   public function chiSiamo() {

        $members = [
            ['name'=>'Alessandro Franco', 'role'=>'Infermiere', 'description'=>'descrizione', 'img'=>'/img/medico2.jpg'],
            ['name'=>'Davide Cariola', 'role'=>'Odontoiatra', 'description'=>'descrizione', 'img'=>'/img/team.jpg'],
            ['name'=>'Edoardo Agus', 'role'=>'Odontoiatra', 'description'=>'descrizione', 'img'=>'/img/supermedico.jpg'],
            ['name'=>'Loris Pozzuoli', 'role'=>'Odontoiatra', 'description'=>'descrizione', 'img'=>'/img/dottorino.jpg'],
        ];
        return view('team', ['members'=>$members]);

    }

    public function servizi($title) {
        
        $services = [
            ['service'=>'Protesi mobili', 'offered'=>'Alessandro Franco ', 'description'=>'descrizione', 'img'=>'/img/stock2.jpg'],
            ['service'=>'Corone dentali', 'offered'=>'Davide Cariola', 'description'=>'descrizione', 'img'=>'/img/dente.jpg'],
            ['service'=>'Impianti dentali', 'offered'=>'Edoardo Agus', 'description'=>'descrizione', 'img'=>'/img/servicee.jpg'],
            ['service'=>'Total clean', 'offered'=>'Loris Pozzuoli', 'description'=>'descrizione', 'img'=>'/img/stock1.jpg'],

        ];

        foreach ($services as $service) {
            if ($service['service'] == $title){
                
                return view('servicesDett', ['service'=>$service]);
            }
        }

        
    } 

    public function servGen(){
        
        $services = [
            ['service'=>'Protesi mobili', 'offered'=>'Alessandro Franco', 'description'=>'descrizione', 'img'=>'/img/stock2.jpg'],
            ['service'=>'Corone dentali', 'offered'=>'Davide Cariola', 'description'=>'descrizione', 'img'=>'/img/dente.jpg'],
            ['service'=>'Impianti dentali', 'offered'=>'Edoardo Agus', 'description'=>'descrizione', 'img'=>'/img/servicee.jpg'],
            ['service'=>'Total clean', 'offered'=>'Loris Pozzuoli', 'description'=>'descrizione', 'img'=>'/img/stock1.jpg'],

        ];

        return view('services', ['services'=>$services]);
    }

    public function schede($name){

        $members = [
            ['name'=>'Alessandro Franco', 'role'=>'Infermiere', 'description'=>'descrizione', 'img'=>'/img/medico2.jpg'],
            ['name'=>'Davide Cariola', 'role'=>'Odontoiatra', 'description'=>'descrizione', 'img'=>'/img/team.jpg'],
            ['name'=>'Edoardo Agus', 'role'=>'Odontoiatra', 'description'=>'descrizione', 'img'=>'/img/supermedico.jpg'],
            ['name'=>'Loris Pozzuoli', 'role'=>'Odontoiatra', 'description'=>'descrizione', 'img'=>'/img/dottorino.jpg'],
        ];
            foreach ($members as $member) {
                if ($member['name'] == $name){
                       
                    return view('schedules', ['member'=>$member]);
                }
            }
           
    }

    public function contacts(){
        return view('contatti');
    }

    public function submit(Request $req){
        $user = $req->input('user');
        $message = $req->input('message');

        $email = $req->input('email');

        $contact = compact('user','message');

        Mail::to($email)->send(new ContactMail($contact));

        return redirect(route('homepage'))->with('message', 'La tua richiesta è stata inoltrata');


    }












    // graffa che chiude public controller 
}


